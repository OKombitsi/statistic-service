package ru.kombitsi.statisticservice.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import ru.kombitsi.statisticservice.entity.Event;
import ru.kombitsi.statisticservice.enumerate.Classifier;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
public class EventServiceTest {

    private EventService eventService;
    private static Event event;

    @Autowired
    public EventServiceTest(final EventService eventService) {
        this.eventService = eventService;
    }

    @BeforeAll
    public static void createEvent(){
        event = new Event();
        event.setId("1");
        event.setName("testevent1");
        event.setDescription("test");
        event.setClassifier(Classifier.TYPE_1);
    }

    @Test
    public void create(){
        eventService.create(event);
        Event event2 = eventService.findById("1");
        assertEquals("testevent1", event2.getName());
    }

    @Test
    public void findAll(){
        Integer pageNumber = 0;
        Integer pageSize = Integer.MAX_VALUE;
        Classifier classifier = Classifier.TYPE_1;
        eventService.create(event);
        assertNotNull(eventService.findAll(pageNumber, pageSize, classifier));
        assertTrue(eventService.findAll(pageNumber, pageSize, classifier).contains(event));
    }
}
