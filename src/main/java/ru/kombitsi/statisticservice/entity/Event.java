package ru.kombitsi.statisticservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import ru.kombitsi.statisticservice.enumerate.Classifier;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name ="event")
public class Event {

    @Id
    private String id = UUID.randomUUID().toString();
    @Column(name="name", nullable = false)
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "date_create")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date dateCreate = new Date();
    @Column(name = "classifier", nullable = false)
    @Enumerated(EnumType.STRING)
    private Classifier classifier;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Classifier getClassifier() {
        return classifier;
    }

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return getId().equals(event.getId()) &&
                getName().equals(event.getName()) &&
                Objects.equals(getDescription(), event.getDescription()) &&
                getDateCreate().equals(event.getDateCreate()) &&
                getClassifier() == event.getClassifier();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getDateCreate(), getClassifier());
    }
}
