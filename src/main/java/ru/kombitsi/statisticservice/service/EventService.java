package ru.kombitsi.statisticservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kombitsi.statisticservice.entity.Event;
import ru.kombitsi.statisticservice.enumerate.Classifier;
import ru.kombitsi.statisticservice.repository.EventRepository;

import java.util.List;

@Service
@Transactional
public class EventService {

    private EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> findAll(final Integer pageNumber, final Integer pageSize, final Classifier classifier) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("dateCreate"));
        return eventRepository.findAllByClassifier(classifier, page);
    }

    public Event create(final Event event) {
        if (eventRepository.findById(event.getId()).isPresent()) {
            throw new IllegalArgumentException("Entity is already exist");
        }
        return eventRepository.save(event);
    }

    public Event findById(final String id){
        if (!eventRepository.findById(id).isPresent()) {
            throw new IllegalArgumentException("Entity does not exist");
        }
        return eventRepository.findById(id).get();
    }
}

