package ru.kombitsi.statisticservice.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kombitsi.statisticservice.entity.Event;
import ru.kombitsi.statisticservice.enumerate.Classifier;
import ru.kombitsi.statisticservice.service.EventService;

import java.util.List;

@RestController
public class EventRestController {

    private EventService eventService;

    @Autowired
    public EventRestController(final EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(value = "/events", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find events by classifier.")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 200, message = "Events has been found."),
            @ApiResponse(code = 404, message = "Events with this classifier doesn't exist.")
    })
    public ResponseEntity<List<Event>> findAll(@RequestParam final Integer pageNumber,
                                               @RequestParam final Integer pageSize,
                                               @RequestParam final Classifier classifier) {
        final List<Event> events;
        try {
            events = eventService.findAll(pageNumber, pageSize, classifier);
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(events);
    }

    @PostMapping(value = "/event", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create event with typing by classifier.")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 201, message = "The event has been created."),
            @ApiResponse(code = 409, message = "The event with this id already exists.")
    })
    public ResponseEntity<Event> create(@RequestBody final Event event) {
        final Event createEvent;
        try {
            createEvent = eventService.create(event);
        } catch (IllegalArgumentException ila) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(createEvent);
    }
}
