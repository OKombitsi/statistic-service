package ru.kombitsi.statisticservice.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kombitsi.statisticservice.entity.Event;
import ru.kombitsi.statisticservice.enumerate.Classifier;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, String> {

    List<Event> findAllByClassifier(Classifier classifierValue, Pageable pageable);
}
