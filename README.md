# STATISTIC SERVICE (BACKEND)

## TECHNOLOGIES

REST, JPA.

## SOFTWARE

- Java 8
- Spring Boot
- Hibernate
- Swagger
- Junit
- Maven 4
- PostgreSQL

## BUILD APPLICATION WITH MAVEN

```bash
mvn clean install
```

## DOCUMENTATION

- Документация веб-сервиса:


[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

- DDL-скрипт базы данных:

```bash
./database-script.ddl
```

## PAGEING AND SORTING
Параметры паджинации передаются пользователем при Get - запросе.
Сортировка результатов осуществлена по дате.

## DEVELOPER

Kombitsi Olga <Kombitsi@mail.ru>  

