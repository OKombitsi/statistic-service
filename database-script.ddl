CREATE USER postgres
    WITH ENCRYPTED PASSWORD 'postgres';

CREATE DATABASE statistic_service
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;